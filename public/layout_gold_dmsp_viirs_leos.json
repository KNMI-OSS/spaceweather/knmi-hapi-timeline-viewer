{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2023-11-04T12:00:00Z",
        "tmax": "2023-11-07T00:00:00Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "GOLD 135.6 nm radiance / LEO orbits",
            "title_color": "white",
            "visible": true,
            "globe_visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 600,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#30133C"
                    }
                ]
            },
            "margins": {
                "top": 20,
                "bottom": 0
            },
            "options_3d": {
                "camera_attached_to": "Earth",
                "x_position": 0.5,
                "camera_position": [4.4616, 0, 4.8642],
                "orbit_controls_enabled": false,
                "render_earth": false,
                "clip_far": 6.7
            },
            "plot_elements": [
                {
                    "title": "GOLD 135.6 nm images",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 6000,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 3,
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "color_map": "Gray",
                        "color_map_domain": {
                            "bounds": [0, 255],
                            "trim": [0, 255]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "gold_oi_1356_emission_10000",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "z_parameter_multiplier": 1,
                        "z_parameter_offset": 0,
                        "z_parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "GOLD coastline image",
                    "visible": true,
                    "plot_options": {
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "plot_type": "static_image"
                    },
                    "data": {
                        "url": "https://acc.spaceweather.knmi.nl/data/gold/static/gold_coastlines.png"
                    },
                    "data_transforms": {}
                },
                {
                    "title": "GOLD quasi-dipole coordinate graticule",
                    "visible": true,
                    "plot_options": {
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "plot_type": "static_image"
                    },
                    "data": {
                        "url": "https://acc.spaceweather.knmi.nl/data/gold/static/gold_graticule_qd.png"
                    },
                    "data_transforms": {}
                },
                {
                    "title": "DMSP-F17 orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#d93C3c",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#d93C3c",
                        "fill-opacity": 0.16,
                        "label": "F17",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_DMSP_F17",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "DMSP-F18 orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#d93C3c",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#d93C3c",
                        "fill-opacity": 0.16,
                        "label": "F18",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_DMSP_F18",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Suomi-NPP orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#00d96c",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#00d96c",
                        "fill-opacity": 0.16,
                        "label": "NPP",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_SUOMI_NPP",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "NOAA-20 orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#00d96c",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#00d96c",
                        "fill-opacity": 0.16,
                        "label": "N20",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_NOAA_20",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "NOAA-21 orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#00d96c",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#00d96c",
                        "fill-opacity": 0.16,
                        "label": "N21",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_NOAA_21",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "label": "A",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.42,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "GRACE-FO A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#7754c8",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#7754c8",
                        "fill-opacity": 0.16,
                        "label": "GRFO",
                        "label_position_x": 0.42,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "orbit_tle_grace_fo_a",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "VIIRS DNB North",
            "title_color": "#ffffff",
            "visible": false,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 500,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "VIIRS DNB images North",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 10800,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 1,
                        "image_margins": {
                            "top": 3,
                            "bottom": 10
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 2,
                        "color_map": "Turbo",
                        "color_map_domain": {
                            "bounds": [0, 500],
                            "trim": [0, 500]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "viirs_dnb_nh",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.15
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "z_parameter_multiplier": 1,
                        "z_parameter_offset": 0,
                        "z_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "VIIRS DNB South",
            "title_color": "#ffffff",
            "visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "hidden",
            "height": 500,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 10,
                "bottom": 30
            },
            "plot_elements": [
                {
                    "title": "VIIRS DNB images South",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 10800,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 1,
                        "image_margins": {
                            "top": 3,
                            "bottom": 10
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 2,
                        "color_map": "Turbo",
                        "color_map_domain": {
                            "bounds": [0, 500],
                            "trim": [0, 500]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "viirs_dnb_sh",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.15
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "z_parameter_multiplier": 1,
                        "z_parameter_offset": 0,
                        "z_parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "DMSP F17 SSUSI SDR swath",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [-90, 90],
                "bounds": [-90, 90]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -90,
                        "y1": 90,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 5,
                "bottom": 5
            },
            "plot_elements": [
                {
                    "title": "DMSP F17 SSUSI SDR swath",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "interval_image",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "dmsp_f17_sdr_swath_lbhs",
                        "parameter": "url",
                        "time_parameter": "time",
                        "baseline_parameter": "time_stop",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.125
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "DMSP F18 SSUSI SDR swath",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [-90, 90],
                "bounds": [-90, 90]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -90,
                        "y1": 90,
                        "color": "#000000"
                    }
                ]
            },
            "margins": {
                "top": 5,
                "bottom": 5
            },
            "plot_elements": [
                {
                    "title": "DMSP F18 swath",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "interval_image",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "dmsp_f18_sdr_swath_lbhs",
                        "parameter": "url",
                        "time_parameter": "time",
                        "baseline_parameter": "time_stop",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.125
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "GFZ-Potsdam Hp30 index and NOAA Kp forecast",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 80,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 600,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {}
                },
                {
                    "title": "NOAA 3-day Kp forecast",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0,
                        "bar_text": false,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ],
                        "fill-opacity": "30%",
                        "stroke-width": "1px",
                        "stroke_colors": [
                            {
                                "threshold": 1000,
                                "color": "#888888"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "kp_index_noaa_geomag_forecast",
                        "parameter": "kp_index",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {}
                }
            ]
        }
    ]
}
