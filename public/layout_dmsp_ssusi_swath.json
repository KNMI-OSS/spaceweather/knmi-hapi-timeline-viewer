{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2022-01-01T00:00:00Z",
        "tmax": "2024-01-01T00:00:00Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "DMSP F17 swath",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [-90, 90],
                "bounds": [-90, 90]
            },
            "y_label": "Cross-track pixels",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 5
            },
            "plot_elements": [
                {
                    "title": "DMSP F17 SSUSI SDR swath",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "interval_image",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "dmsp_f17_sdr_swath_lbhs",
                        "parameter": "url",
                        "time_parameter": "time",
                        "baseline_parameter": "time_stop",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.125
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "DMSP F18 SSUSI SDR swath",
            "title_color": "#ffffff",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [-90, 90],
                "bounds": [-90, 90]
            },
            "y_label": "Cross-track pixels",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 5
            },
            "plot_elements": [
                {
                    "title": "DMSP F18 swath",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "interval_image",
                        "stroke": "#006CD9",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#006CD9",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "dmsp_f18_sdr_swath_lbhs",
                        "parameter": "url",
                        "time_parameter": "time",
                        "baseline_parameter": "time_stop",
                        "cadence_alternative_datasets_mode": "off",
                        "parameter_multiplier": 1,
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.125
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Hp30 index",
            "visible": true,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"],
                "mirror": false
            },
            "y_label": "Hp30 index",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 12,
                "bottom": 0
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {}
                }
            ],
            "legend_visible": false
        },
        {
            "title": "10.7 cm solar radio flux",
            "visible": true,
            "ydomain": {
                "default": [60, 250],
                "bounds": [0, 500]
            },
            "y_label": "F10.7 (sfu)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [
                    {
                        "y": 100
                    },
                    {
                        "y": 200
                    },
                    {
                        "y": 300
                    }
                ],
                "annot_text_items": []
            },
            "margins": {
                "top": 30,
                "bottom": 42
            },
            "plot_elements": [
                {
                    "title": "F10.7",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#000000",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": 0.25,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "0",
                        "baseline_stroke-opacity": "0"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "f10_7",
                        "parameter": "f10_7",
                        "time_parameter": "time",
                        "baseline_parameter": "f10_7a",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "F10.7 81-day running mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#000000",
                        "stroke-width": "1",
                        "stroke-opacity": "1",
                        "fill": "#445588",
                        "fill-opacity": 0.25,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0,
                        "baseline_stroke": "#000000",
                        "baseline_stroke-width": "2",
                        "baseline_stroke-opacity": "1"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "f10_7",
                        "parameter": "f10_7a",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        }
    ]
}
