{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2023-04-23T12:00:00.000Z",
        "tmax": "2023-04-24T00:00:00.000Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "Swarm orbits and orbit altitude",
            "visible": true,
            "legend_visible": false,
            "globe_visible": true,
            "ydomain": {
                "default": [400, 550],
                "bounds": [200, 600]
            },
            "y_label": "Altitude (km)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "options_3d": {
                "camera_attached_to": "Sun",
                "x_position": 0.5,
                "render_earth": true
            },
            "plot_elements": [
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.25,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B altitude long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#A070DC",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude long-term min/max",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "area",
                        "stroke": "#74a7fe",
                        "stroke-width": 0,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "parameter"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_max",
                        "time_parameter": "time",
                        "baseline_parameter": "height_min",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B altitude long-term mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#A070DC",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C altitude long-term mean",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 1,
                        "stroke-opacity": "1",
                        "fill": "#74a7fe",
                        "fill-opacity": 0.1,
                        "mirror_zero_x": false,
                        "curve_type": "step",
                        "baseline_mode": "constant"
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_aggregate",
                        "parameter": "height_mean",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B altitude",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C altitude",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc",
                        "parameter": "height",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 0.001,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "Swarm electron density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 3],
                "bounds": [0, 6],
                "mirror": false
            },
            "y_label": "Ne (10⁶/cm³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 33,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm B Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efic_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B WAM-IPE modeled Ne",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#A070DC",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmb",
                        "parameter": "electron_density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C WAM-IPE modeled Ne",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#71b8ff",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmc",
                        "parameter": "electron_density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm mVTEC",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 80],
                "bounds": ["0", 100],
                "mirror": false
            },
            "y_label": "Ne (10⁶/cm³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 33,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm B IPDxIRR mVTEC data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://vires.services/hapi",
                        "dataset": "SW_OPER_IPDBIRR_2F",
                        "parameter": "mVTEC",
                        "time_parameter": "Timestamp",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C IPDxIRR mVTEC data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://vires.services/hapi",
                        "dataset": "SW_OPER_IPDCIRR_2F",
                        "parameter": "mVTEC",
                        "time_parameter": "Timestamp",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B WAM-IPE modeled mVTEC",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#A070DC",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmb",
                        "parameter": "VTEC_above",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C WAM-IPE modeled mVTEC",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#71b8ff",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmc",
                        "parameter": "VTEC_above",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Swarm electron temperature",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 6000],
                "bounds": [0, 10000],
                "mirror": false
            },
            "y_label": "Te (K)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 130,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 33,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm B Te 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp",
                        "parameter": "Te",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Te 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efic_lp",
                        "parameter": "Te",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B WAM-IPE modeled Te",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#A070DC",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmb",
                        "parameter": "electron_temperature",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C WAM-IPE modeled Te",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#71b8ff",
                        "stroke-width": 6,
                        "stroke-opacity": 0.5,
                        "fill": "#888888",
                        "fill-opacity": 0,
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": "constant",
                        "baseline": 0
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "wam_ipe_ne_swarmc",
                        "parameter": "electron_temperature",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                }
            ]
        },
        {
            "title": "Hp30 index",
            "visible": true,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "Hp30 index",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "parameter_multiplier": 1,
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {}
                }
            ],
            "legend_visible": true
        }
    ]
}
