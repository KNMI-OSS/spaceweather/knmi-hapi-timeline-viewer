{
    "options": {
        "fill_vertical": true,
        "fill_horizontal": true,
        "zoom_detail_factor": 1
    },
    "default_zoom": {
        "tmin": "2023-04-01T20:00:00.000Z",
        "tmax": "2023-04-02T00:00:00.000Z"
    },
    "global_margins": {
        "top": 0,
        "bottom": 0,
        "left": 65,
        "right": 30
    },
    "subplots": [
        {
            "title": "GOLD atomic oxygen emissions",
            "title_color": "white",
            "visible": true,
            "globe_visible": true,
            "ydomain": {
                "default": [-350, 350],
                "bounds": [-500, 500]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 300,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": [],
                "annot_background_colors": [
                    {
                        "y0": -500,
                        "y1": 500,
                        "color": "#30133C"
                    }
                ]
            },
            "margins": {
                "top": 20,
                "bottom": 0
            },
            "options_3d": {
                "camera_attached_to": "Earth",
                "x_position": 0.5,
                "camera_position": [4.4616, 0, 4.8642],
                "orbit_controls_enabled": false,
                "render_earth": false,
                "clip_far": 6.7
            },
            "plot_elements": [
                {
                    "title": "GOLD 135.6 nm images",
                    "visible": true,
                    "plot_options": {
                        "max_time_diff_before_seconds": 2000,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 3,
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "color_map": "Gray",
                        "color_map_domain": {
                            "bounds": [0, 255],
                            "trim": [0, 255]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "gold_oi_1356_emission",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "z_parameter_multiplier": 1,
                        "z_parameter_offset": 0,
                        "z_parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "GOLD IBP model contours (Stolle et al)",
                    "visible": false,
                    "plot_options": {
                        "max_time_diff_before_seconds": 2000,
                        "max_time_diff_after_seconds": 0,
                        "max_retain_images": 1,
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "color_map": "Gray",
                        "color_map_domain": {
                            "bounds": [0, 255],
                            "trim": [0, 255]
                        },
                        "reverse_color_map": false,
                        "symbol_type": "circle",
                        "symbol_height": 5,
                        "symbol_width": 5,
                        "plot_type": "epoch_image",
                        "stroke": "#000000",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "gold_ibpmodel_contours",
                        "parameter": "url",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 3
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "z_parameter_multiplier": 1,
                        "z_parameter_offset": 0,
                        "z_parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "GOLD coastline image",
                    "visible": true,
                    "plot_options": {
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "plot_type": "static_image"
                    },
                    "data": {
                        "url": "https://acc.spaceweather.knmi.nl/data/gold/static/gold_coastlines.png"
                    },
                    "data_transforms": {}
                },
                {
                    "title": "GOLD quasi-dipole coordinate graticule",
                    "visible": false,
                    "plot_options": {
                        "image_margins": {
                            "top": 0,
                            "bottom": 0
                        },
                        "marker_color": "#ffffff",
                        "aspect_ratio": 1,
                        "plot_type": "static_image"
                    },
                    "data": {
                        "url": "https://acc.spaceweather.knmi.nl/data/gold/static/gold_graticule_qd.png"
                    },
                    "data_transforms": {}
                },
                {
                    "title": "Swarm A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "label": "A",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.58,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.42,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "BRIK-II orbit",
                    "visible": false,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#e2ad1d",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#e2ad1d",
                        "fill-opacity": 0.16,
                        "label": "B-II",
                        "label_position_x": 0.5,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "brik_ii_orbit_tle",
                        "parameter": "x_itrf",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "Swarm orbits",
            "visible": false,
            "legend_visible": false,
            "globe_visible": true,
            "ydomain": {
                "default": [400, 550],
                "bounds": [200, 600]
            },
            "y_label": "Altitude (km)",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "hidden",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "options_3d": {
                "camera_attached_to": "Earth",
                "x_position": 0.5,
                "camera_position": [4.4616, 0, 4.8642],
                "orbit_controls_enabled": false,
                "render_earth": false
            },
            "plot_elements": [
                {
                    "title": "Swarm A orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "label": "A",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_moda_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm B orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "label": "B",
                        "label_position_x": 0.75,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modb_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                },
                {
                    "title": "Swarm C orbit",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "orbit_on_globe",
                        "stroke": "#74a7fe",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#74a7fe",
                        "fill-opacity": 0.16,
                        "label": "C",
                        "label_position_x": 0.25,
                        "label_position_y": 0.5,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_modc_sc_PT30S",
                        "parameter": "x_gcrs",
                        "time_parameter": "time",
                        "x_coordinate_parameter": "x_gcrs",
                        "y_coordinate_parameter": "y_gcrs",
                        "z_coordinate_parameter": "z_gcrs",
                        "cadence_alternative_datasets_mode": "off",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "coordinate_parameter_multiplier": 0.0001569612306
                    }
                }
            ]
        },
        {
            "title": "Swarm OPER electron density",
            "visible": true,
            "legend_visible": true,
            "ydomain": {
                "default": [0, 3],
                "bounds": [0, 6]
            },
            "y_label": "Ne (10⁶/cm³)",
            "scale": "linear",
            "ytick_format": "auto",
            "xaxis_annotation": "hidden",
            "yaxis_annotation": "visible",
            "height": 150,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 20,
                "bottom": 10
            },
            "plot_elements": [
                {
                    "title": "Swarm A Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#006CD9",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#006CD9",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efia_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm B Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#A070DC",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#A070DC",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efib_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "Swarm C Ne 2Hz OPER data",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "line",
                        "stroke": "#70B8FF",
                        "stroke-width": 2.5,
                        "stroke-opacity": 1,
                        "fill": "#70B8FF",
                        "fill-opacity": 0.16,
                        "mirror_zero_x": false,
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "sw_oper_efic_lp",
                        "parameter": "Ne",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "zoom_detail_factor": 1.0
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false
                    }
                },
                {
                    "title": "BRIK-II ScinMon Ne",
                    "visible": true,
                    "plot_options": {
                        "stroke": "#e2ad1d",
                        "stroke-width": "2",
                        "stroke-opacity": "1",
                        "fill": "#e2ad1d",
                        "fill-opacity": "1",
                        "mirror_zero_x": false,
                        "plot_type": "line",
                        "curve_type": "linear",
                        "baseline_mode": ""
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "brik_ii_electron_density",
                        "parameter": "electron_density",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 4
                    },
                    "data_transforms": {
                        "parameter_multiplier": 1e-6,
                        "parameter_offset": 0,
                        "parameter_offset_remove_mean": false,
                        "time_offset_sec": 0
                    }
                }
            ]
        },
        {
            "title": "GFZ-Potsdam Hp30 index and NOAA Kp forecast",
            "visible": true,
            "legend_visible": false,
            "ydomain": {
                "default": [0, "9"],
                "bounds": ["0", "15"]
            },
            "y_label": "",
            "scale": "linear",
            "ytick_format": "d",
            "xaxis_annotation": "visible",
            "yaxis_annotation": "visible",
            "height": 50,
            "annot_options": {
                "annot_horizontal_grid_lines": [],
                "annot_text_items": []
            },
            "margins": {
                "top": 10,
                "bottom": 40
            },
            "plot_elements": [
                {
                    "title": "Hp30 index",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "stroke": "#000000",
                        "stroke-width": "0",
                        "stroke-opacity": "1",
                        "fill": "#888888",
                        "fill-opacity": "1",
                        "curve_type": "linear",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0.2,
                        "bar_text": true,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "hp30_index",
                        "parameter": "Hp30",
                        "time_parameter": "time",
                        "cadence_alternative_datasets_mode": "x_relations",
                        "refetch_interval_seconds": 600,
                        "zoom_detail_factor": 2
                    },
                    "data_transforms": {}
                },
                {
                    "title": "NOAA 3-day Kp forecast",
                    "visible": true,
                    "plot_options": {
                        "plot_type": "bars",
                        "bar_offset_seconds": 1800,
                        "bar_padding": 0,
                        "bar_text": false,
                        "fill_colors": [
                            {
                                "threshold": 4.6,
                                "color": "#88CC66"
                            },
                            {
                                "threshold": 5.6,
                                "color": "#FFF033"
                            },
                            {
                                "threshold": 6.6,
                                "color": "#FFBB44"
                            },
                            {
                                "threshold": 7.6,
                                "color": "#DD9922"
                            },
                            {
                                "threshold": 8.6,
                                "color": "#FF2020"
                            },
                            {
                                "threshold": 9.6,
                                "color": "#A02020"
                            },
                            {
                                "threshold": 1000,
                                "color": "#300000"
                            }
                        ],
                        "fill-opacity": "30%",
                        "stroke-width": "1px",
                        "stroke_colors": [
                            {
                                "threshold": 1000,
                                "color": "#888888"
                            }
                        ]
                    },
                    "data": {
                        "server": "https://hapi.spaceweather.knmi.nl/hapi",
                        "dataset": "kp_index_noaa_geomag_forecast",
                        "parameter": "kp_index",
                        "time_parameter": "time",
                        "issue_time_parameter": "timetag_issue",
                        "cadence_alternative_datasets_mode": "off",
                        "refetch_interval_seconds": 86400,
                        "zoom_detail_factor": 0.5
                    },
                    "data_transforms": {}
                }
            ]
        }
    ]
}
