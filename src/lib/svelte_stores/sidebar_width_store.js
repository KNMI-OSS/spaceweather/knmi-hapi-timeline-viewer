import { writable } from "svelte/store";
// This store allows different Svelte components to access information about the
// window and panel geometry
export const innerWidth = writable(600);
export const sidebar_width = writable(400);
export const sidebar_width_tab_labels = 465;
