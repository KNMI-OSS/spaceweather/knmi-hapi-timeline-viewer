import { writable } from "svelte/store";

export const notification_modal_visible = writable(false);
export const notification_modal_content = writable("No contents");
