import { writable } from "svelte/store";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";
dayjs.extend(utc);
import {
    timeDomain,
    parse_time_or_duration_string,
} from "./timedomain_store.js";

// This file contains the code for storing all the layout

let initial_layout = "layout_real_time_space_weather.json";

let t0, t1;
// t0 = parse_time_or_duration_string($layoutQuery.data.default_zoom.tmin),
// t1 = parse_time_or_duration_string($layoutQuery.data.default_zoom.tmax);

// Check if the current URL has an ?layout=... item in the query string
let urlParams = new URLSearchParams(window.location.search);
if (urlParams.has("layout")) {
    initial_layout = "layout_" + urlParams.get("layout") + ".json";
}

// Check if the current URL has the layout, start and/or end time provided
// in the URL hash (the section of the URL after the #-sign, like in .../viewer/#start=...)
if (window.location.hash !== "") {
    let urlParams = new URLSearchParams(window.location.hash.substring(1));
    if (urlParams.has("layout")) {
        initial_layout = urlParams.get("layout");
    }
    if (urlParams.has("start")) {
        let start = urlParams.get("start");
        t0 = dayjs.utc(start);
    }
    if (urlParams.has("end")) {
        let end = urlParams.get("end");
        t1 = dayjs.utc(end);
    }
}

export const layout_file = writable(initial_layout);
export const layout = writable();
