import { writable } from "svelte/store";

export const show_info = writable(false);
export const left_pane_api = writable();
export const right_pane_api = writable();
export const left_pane_collapsed = writable(true);
